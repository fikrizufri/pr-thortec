<?php
namespace App\Repository\Photo;

use Thortech\Interfaces\RepositoryInterface;

/**
 * interface ini untuk mengatur segala API untuk mengakses data user dari
 * database/data repositori.
 */
interface PhotoRepositoryInterface extends RepositoryInterface
{
}
